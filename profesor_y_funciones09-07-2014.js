/*Estructura de el objeto profesor
Usa a su ver las estructuras alumnos y departamento
*/

departamento={"clave":"sccc","nombre":"Departamento de computacion"};

alumnos=[
    {
        "no. control":"10160844","nombre_completo":"Jimenez Alvarez Jasiel"
    },
    {
        "no. control":"10164732","nombre_completo":"Diaz Jimenez Magaly"
    },
    {
        "no. control":"10167899","nombre_completo":"Vasquez Pinacho Daniel"
    },
    {
        "no. control":"1016666","nombre_completo":"JiWaw adfoo Juanito"
    }
    
    
    ];

obj_profesor={
"clave":"123ert",
"nombre":"Hernandez Blas Antonio",
"departamento":departamento,
"grupos":[
    {
    "clave":"6uyh","nombre":"ISC","aula":"I10","horario":"7:00-8:00","alumnos":alumnos    
    },
    {
    "clave":"6ufg","nombre":"ISA","aula":"I9","horario":"7:00-11:00","alumnos":alumnos
    },
    {
    "clave":"6ufghhj","nombre":"ISD","aula":"I5","horario":"7:00-10:00","alumnos":alumnos
    },
    {
    "clave":"6ufghhj","nombre":"ISD","aula":"I5","horario":"7:00-10:00","alumnos":[
         {
          "no. control":"10160844","nombre_completo":"Jimenez Alvarez Jasiel"
         },
         {
          "no. control":"10164732","nombre_completo":"Diaz Jimenez Magaly"
         }
        ]
    }
    ]

}

/*Funcion que devuelve el número de alumnos que tiene un profesor en todos sus grupos*/
function total_alumnos(profesor)
{
    var total=0;
    for(var i=0;i<profesor["grupos"].length;i++)
    {
        if(profesor["grupos"][i]["alumnos"]){
            total=total+profesor["grupos"][i]["alumnos"].length;
        }
    }
    return total;
}

/*Función que cuenta el número de alumnos en un determinado grupo n indica el grupo*/
function numero_alumnos(profesor,n)
{
    if(profesor["grupos"][n])
    {
        return profesor["grupos"][n]["alumnos"].length;
    }
    return 0;
}

/*Prueba de las dos funciones*/

console.log(numero_alumnos(obj_profesor,3));

console.log(total_alumnos(obj_profesor));
